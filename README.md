### How to set up local environment? ###

* Checkout git repository in your local machine
* Run command:
``
composer:install
``
* Create empty DB and configure DATABASE_URL parameter in .env file
* Migrate DB:
``
php bin/console d:m:m
``
* Run the built-in web server:
``
php bin/console server:run
``
* Enjoy. The /admin login - username: tomas, password: abc123
