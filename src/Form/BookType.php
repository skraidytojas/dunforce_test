<?php

namespace App\Form;

use App\Entity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('author')
            ->add('coverPicture', null, ['required' => false])
            ->add('isbn', null, ['label' => 'ISBN'])
            ->add('tags', TextType::class, ['label' => 'Tags. (Separate with commas, no spaces.)'])
            ->add('save', SubmitType::class, array('label' => 'Continue'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Entity\Book::class,
        ]);
    }
}