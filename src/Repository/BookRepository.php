<?php

namespace App\Repository;

use App\Entity;

class BookRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param int $offset
     * @param int $limit
     * @return Entity\Book[]|array
     */
    public function getAllBooks($offset = 0, $limit = 100)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Book::class)->createQueryBuilder('b')
            ->orderBy('b.title', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    public function deleteBook(Entity\Book $book)
    {
        $em = $this->getEntityManager();

        $em->remove($book);
        $em->flush();

        return true;
    }
}