<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity;
use App\Form;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use App\Service\BookCoverPictureUploader;
use Symfony\Component\HttpFoundation\File\File;

/**
 * COMMENT FROM TOMAS: some code duplication among controller actions is visible. I left that for the sake of simplicity and since it's only two actions.
 * If there were more actions and if it was for real-world use, I'd introduce some abstraction layers to make code leaner. Something like standardized CRUD actions.
 */
class AdminController extends Controller
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var BookCoverPictureUploader */
    protected $bookCoverUploader;

    public function __construct
    (
        EntityManagerInterface $em,
        BookCoverPictureUploader $bookCoverUploader
    )
    {
        $this->em = $em;
        $this->bookCoverUploader = $bookCoverUploader;
    }

    public function manageAuthor(Request $request, $action, $id = null)
    {
        $author = 'create' === $action ? new Entity\Author : $this->em->getRepository(Entity\Author::class)->find($id);

        if (!$author) {
            throw $this->createNotFoundException();
        }

        if ('delete' === $action) {
            $this->em->getRepository(Entity\Author::class)->deleteAuthor($author);
            $this->addFlash("notice", "Author $action successful!");

            return $this->redirectToRoute('home');
        }

        $form = $this->createForm(Form\AuthorType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $author = $form->getData();

            if ('create' === $action) {
                $this->em->persist($author);
            }

            try {
                $this->em->flush();
                $this->addFlash("notice", "Author $action successful!");

                return $this->redirectToRoute('home');

            } catch (UniqueConstraintViolationException $e) {
                $this->addFlash("error", "Author already exists!");
            }
        }

        return $this->render('./Admin/manageAuthor.html.twig', [
            'form' => $form->createView(),
            'action' => $action,
        ]);
    }

    public function manageBook(Request $request, $action, $id = null)
    {
        $book = 'create' === $action ? new Entity\Book : $this->em->getRepository(Entity\Book::class)->find($id);

        if (!$book) {
            throw $this->createNotFoundException();
        }

        if ('delete' === $action) {
            $this->em->getRepository(Entity\Book::class)->deleteBook($book);
            $this->addFlash("notice", "Book $action successful!");

            return $this->redirectToRoute('home');
        }

        $originalCoverPictureFileName = $book->getCoverPicture();

        if ('edit' === $action && $book->getCoverPicture()) {
            $book->setCoverPicture(
                new File($this->getParameter('book_cover_directory') . '/' . $book->getCoverPicture())
            );
        }

        $form = $this->createForm(Form\BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ('create' === $action) {
                $this->em->persist($book);
            }

            if ( ($file = $book->getCoverPicture()) ) {
                $fileName = $this->bookCoverUploader->upload($file);
                $book->setCoverPicture($fileName);
            } else {
                $book->setCoverPicture($originalCoverPictureFileName);
            }

            $this->em->flush();
            $this->addFlash("notice", "Book $action successful!");

            return $this->redirectToRoute('home');
        }

        return $this->render('./Admin/manageBook.html.twig', [
            'form' => $form->createView(),
            'action' => $action,
            'book' => $book,
        ]);
    }
}