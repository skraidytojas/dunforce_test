<?php

namespace App\Repository;

use App\Entity;

class AuthorRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param int $offset
     * @param int $limit
     * @return Entity\Author[]|array
     */
    public function getAllAuthors($offset = 0, $limit = 100)
    {
        $em = $this->getEntityManager();
        $qb = $em->getRepository(Entity\Author::class)->createQueryBuilder('a')
            ->orderBy('a.lastName', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    public function deleteAuthor(Entity\Author $author)
    {
        $em = $this->getEntityManager();

        $em->remove($author);
        $em->flush();

        return true;
    }
}