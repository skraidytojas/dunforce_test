<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity;

class MainController extends Controller
{
    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    public function home()
    {
        $authors = $this->em->getRepository(Entity\Author::class)->getAllAuthors(0, 100);
        $books = $this->em->getRepository(Entity\Book::class)->getAllBooks(0, 100);

        return $this->render('./Main/home.html.twig', [
            'authors' => $authors,
            'books' => $books,
        ]);
    }
}