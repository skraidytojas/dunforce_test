<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity;

class ListBooksOrAuthors extends Command
{

    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:list')
            ->addArgument('entity', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        switch ($input->getArgument('entity')) {
            case 'book':
                $output->writeln('Listing Books (up to 100):');
                foreach ($this->em->getRepository(Entity\Book::class)->getAllBooks(0, 100) as $book) {
                    $output->writeln($book);
                }
                break;
            case 'author':
                $output->writeln('Listing Authors (up to 100):');
                foreach ($this->em->getRepository(Entity\Author::class)->getAllAuthors(0, 100) as $author) {
                    $output->writeln($author);
                }
                break;
            default:
                $output->writeln('Argument value (entity name) is not supported.');
        }
    }
}