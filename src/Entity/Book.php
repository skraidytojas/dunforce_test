<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="`book`")
 * @ORM\Entity(repositoryClass="App\Repository\BookRepository")
 */
class Book
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Title is missing")
     */
    protected $title;

    /**
     * @var Author|null
     *
     * @ORM\ManyToOne(targetEntity="Author", inversedBy="books")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank(message="Author is missing")
     */
    protected $author;

    /**
     * @ORM\Column(name="cover_picture", type="string", nullable=true)
     *
     * @Assert\Image()
     */
    protected $coverPicture;

    /**
     * @var string|null
     *
     * @ORM\Column(name="isbn", type="string", length=255, nullable=true)
     */
    protected $isbn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tags", type="text", nullable=true)
     */
    protected $tags;

    public function __toString()
    {
        return $this->getTitle() . ' - ' . $this->getAuthor();
    }

    /**
     * CUSTOM METHODS:
     */


    /**
     * @param bool $asArray
     * @return array|null|string
     */
    public function getTags($asArray = false)
    {
        if ($asArray) {
            return $this->tags ? explode(',', $this->tags) : [];
        }
        return $this->tags;
    }


    /**
     * GETTERS AND SETTERS:
     */

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Book
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     * @return Book
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Author|null
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param Author|null $author
     * @return Book
     */
    public function setAuthor($author)
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCoverPicture()
    {
        return $this->coverPicture;
    }

    /**
     * @param mixed $coverPicture
     * @return Book
     */
    public function setCoverPicture($coverPicture)
    {
        $this->coverPicture = $coverPicture;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * @param null|string $isbn
     * @return Book
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;
        return $this;
    }

    /**
     * @param string|null $tags
     * @return Book
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }
}